pragma solidity 0.6.3;

contract Contract1{
    mapping(address => uint32) public balances;
    address payable wallet;
    
    constructor(address payable _wallet) public{
        wallet = _wallet;
    }
    
    function buyToken() public payable{
        //buy a token
        balances[msg.sender] +=1;
        //send ether to a wallet
        
        emit Purchase(msg.sender, 1);
    }
    
    receive () external payable{
        wallet.transfer(msg.value);
    }
    
    fallback () external payable{
        buyToken();
    }
    
    event Purchase(
        //indexed allows you to search for specific events using the indexed variable as a parameter as filters. Is only relevant to logged events
        address indexed _buyer,
        uint256 _amount
    );
}

abstract contract Contract2 is Contract1{
    //contract2 inherits from contract1
    //abstract means it must be declared elsewhere (like in Java)
}
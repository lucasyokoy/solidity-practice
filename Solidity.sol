pragma solidity 0.6.3;

contract MyContract{
    string public value;
    string public constant c = 'constant';
    bool public mybool = true;
    int public integer = 1;
    uint public uinteger = 1;
    //no support for float
    //public means any user can access it: it's value is registered on the blockchain itself. Public variables have their get() methods automatically created
    //enum is a special data structue from solidity that keeps track of values in an enumerated list (State.Waiting = 0)
    enum State {Waiting, Ready, Active}
    State public state;
    uint256 public peopleCount = 0;
    struct Person{
        uint256 id;
        string _firstName;
        string _lastName;
    }
    //Person[] public people;
    //mapping makes a key with the associated value to pair each element with. in order to call a person, use people[key]
    //like in a python dictionary, except both the keys and the values can be of any type
    mapping(uint => Person) public people;
    //addres is a datatype inside of solidity and it specifies the address of an account
    address owner;
    
    //you can make custom modifiers as well
    //this one verifies if who called the funcion is the owner of the contract. Only if it is, the funcion is executed.
    //Otherwise it throws an errir with the given message.
    modifier onlyOwner() {
        //msg refers to the metadata of who called the function.
        require(msg.sender == owner,"Function called from unauthorized user.");
        _;
    }
    
    //after the declaration of the function, there are the modifiers. Modifiers are executed before the function, when the function is called
    //ex: public
    function addPerson(string memory _firstName, string memory _lastName) public onlyOwner{
        peopleCount += 1;
        people[peopleCount] = Person(peopleCount,_firstName,_lastName);
    }
    
    constructor() public{
        owner = msg.sender;
        value = "Initial Value";
        state = State.Waiting;
    }
    
    //a public function can be called by anyone
    function activate() public{
        state = State.Active;
    }
    
    function isActive() public view returns(bool){
        return state == State.Active;
    }
    
    /*function get() public view returns(string){
        return value;
    }
    view is a function which returns a value that has't been changed in the function*/
    
    function set(string memory _value) public{ //public means it's available for any user to see and call
        value = _value;
    }
    
    //Internal functions can only be accessed from other fuctions within the contract, not directly by the user
    /*For further information on acces modifiers: (https://solidity.readthedocs.io/en/v0.6.3/contracts.html)
    from stackoverflow:
    public - all can access
    external - Cannot be accessed internally, only externally (If you know the function is only going to be called externally use external, as it's cheaper)
    internal - only this contract and contracts deriving from it can access
    private - can be accessed only from this contract
    */
    function privateFunction() internal returns(string memory){
        peopleCount += 1;
        return "Private Function";
    }
    
}
